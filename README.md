#Creative Sass Components

Check here: http://sass-components.herokuapp.com/ for more info and setup instructions

A simple to use library of simple Sass Components to use to give a bit of animated effect to your website/project.

Most animations are css driven, some require js class switching, theres a few small js libraries included that I wrote, you can however probably replace most of these with your own, or a framework.

both the sass files and the minified css are included, as well as the original js and the minified js for your use.
